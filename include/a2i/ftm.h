// SPDX-FileCopyrightText: 2024, Alejandro Colomar <alx@kernel.org>
// SPDX-License-Identifier: LGPL-3.0-only WITH LGPL-3.0-linking-exception


#ifndef INCLUDE_A2I_FTM_H_
#define INCLUDE_A2I_FTM_H_


#if !defined(A2I_FTM_INLINE)
# define A2I_FTM_INLINE  1
#endif


#endif  // include guard
