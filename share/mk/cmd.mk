# Copyright 2022-2024 Alejandro Colomar <alx@kernel.org>
# SPDX-License-Identifier: LGPL-3.0-only WITH LGPL-3.0-linking-exception


ifndef MAKEFILE_CMD_INCLUDED
MAKEFILE_CMD_INCLUDED := 1


CD      := cd
CP      := cp
CUT     := cut
ECHO    := echo
FIND    := find
GIT     := git
GREP    := grep
LN      := ln
MKDIR   := mkdir
RM      := rm
SED     := sed
SORT    := sort
TAC     := tac
TOUCH   := touch
XARGS   := xargs


endif  # include guard
